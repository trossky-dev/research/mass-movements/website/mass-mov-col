import { Component, OnInit, } from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';


declare var $: any;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  ToHome=true

  location: Location;


  constructor(location: Location,
    private router:Router,
    ) { 
      this.location = location;

      

    }

  ngOnInit() {

  }

  goToHome(){
    if(this.ToHome){
      this.ToHome=false;
      
    }

  }



}
