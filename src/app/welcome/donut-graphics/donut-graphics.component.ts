import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-donut-graphics',
  templateUrl: './donut-graphics.component.html',
  styleUrls: ['./donut-graphics.component.css']
})
export class DonutGraphicsComponent implements OnInit {

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  
  constructor() { }

  ngOnInit() {
    var data = [
      {
        y: [0, 1, 1, 2, 3, 5, 8, 13, 21],
        boxpoints: 'all',
        jitter: 0.3,
        pointpos: -1.8,
        type: 'box'
      }
    ];
    }

   // Doughnut
   public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
   public doughnutChartData:number[] = [350, 450, 100];
   public doughnutChartType:string = 'doughnut';
  
   // events
   public chartClicked(e:any):void {
     console.log(e);
   }
  
   public chartHovered(e:any):void {
     console.log(e);
   }

}
