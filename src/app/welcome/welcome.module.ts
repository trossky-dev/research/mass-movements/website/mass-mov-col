import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { ChartsModule } from 'ng2-charts';


import { WelcomeRouting } from './welcome.routing';

import 'hammerjs';


/*
************************************************
*    Material modules for app
*************************************************
*/

import { MaterialModule } from '../app.material';

/*
************************************************
*     principal component
*************************************************
*/
import { AboutComponent } from './about/about.component';
import { CarouselComponent } from './carousel/carousel.component';
import { ContactComponent } from './contact/contact.component';
import { IndexComponent } from './index/index.component';
import { GraphicsComponent } from './graphics/graphics.component';
import { BarGraphicsComponent } from './bar-graphics/bar-graphics.component';
import { DonutGraphicsComponent } from './donut-graphics/donut-graphics.component';

/*
************************************************
*     modules of  your app
*************************************************
*/


/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/


@NgModule({
  imports: [

    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule,
    PdfViewerModule,
    ChartsModule,
  ],
  declarations: [AboutComponent, CarouselComponent, ContactComponent, IndexComponent, GraphicsComponent, BarGraphicsComponent, DonutGraphicsComponent],
  exports:[IndexComponent ]
})
export class WelcomeModule { }
