import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Sistema Predictivo de Movimientos en Masa.';
  subtitle = 'En el Departamento de Santander.';

  autor='Luis Fernando García Q.'
  study='Est. Ing. Sistemas Y Computación.'

  
  
}
