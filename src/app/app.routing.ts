import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

/***************************************************
 * Llamar el Componente que se muestra por defecto *
 ***************************************************/
//import { IndexComponent } from './welcome/index/index.component'

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { WelcomeRouting }  from './welcome/welcome.routing'
import { StatisticRouting }  from './statistics/statistics.routing'



export const AppRouting: Routes = [
  // { path: '',          redirectTo: '', pathMatch: 'full' }
  ...WelcomeRouting,
  ...StatisticRouting,
  { path: '*', redirectTo:'/',pathMatch:'full'}
];
