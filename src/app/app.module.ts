import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { PdfViewerModule } from 'ng2-pdf-viewer';

//import { MaterialModule } from '@angular/material';
import { MaterialModule } from './app.material';
import 'hammerjs';


import { AppComponent } from './app.component';

/*
************************************************
*     modules of  your app
*************************************************
*/
import {  WelcomeModule } from "./welcome/welcome.module";
import { StatisticsModule }  from './statistics/statistics.module'


import { AppRouting } from './app.routing';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PdfViewerModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(AppRouting),
    MaterialModule,
    WelcomeModule,
    StatisticsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
