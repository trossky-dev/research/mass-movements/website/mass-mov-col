import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';
declare let d3: any;

@Component({
  selector: 'app-graphics',
  templateUrl: './graphics.component.html',
  styleUrls: ['./graphics.component.css', '../../../../node_modules/nvd3/build/nv.d3.css'],
  encapsulation: ViewEncapsulation.None
})
export class GraphicsComponent implements OnInit {
  changeView=true;
  flags=[false,true,false,false,false]

  //Historic
  barChartDataGlobal:any[]
  public barChartLabelsGlobal:string[]
  
  //locals
  //case 1
  barChartDataCase1:any[]
  public barChartLabelsCase1:string[]
  doughnutChartLabelsCase1:string[] = ['5. Muy Alta', '4. Alta', '3. Media', '2. Baja'];
  doughnutChartDataCase1:number[] = [22, 707, 686,3];
  //case 2
  barChartDataCase2:any[]
  public barChartLabelsCase2:string[]
  doughnutChartLabelsCase2:string[] = ['5. Muy Alta', '4. Alta', '3. Media', '2. Baja'];
  doughnutChartDataCase2:number[] = [22, 707, 686,3];

  // case 3
  barChartDataCase3:any[]
  public barChartLabelsCase3:string[]
  doughnutChartLabelsCase3:string[] = ['5. Muy Alta', '4. Alta', '3. Media', '2. Baja'];
  doughnutChartDataCase3:number[] = [22, 707, 686,3];
  //case 4
  barChartDataCase4:any[]
  public barChartLabelsCase4:string[]
  doughnutChartLabelsCase4:string[] = ['5. Muy Alta', '4. Alta', '3. Media', '2. Baja'];
  doughnutChartDataCase4:number[] = [22, 707, 686,3];


  navLinks=[
    {
      "path":"./",
      "label":"2000-2005"
    },
    {
      "path":"./",
      "label":"2006-2010"
    },
    {
      "path":"./",
      "label":"2011-2012"
    },
    {
      "path":"./",
      "label":"2013-2014"
    }
  ]


  constructor() { }

  ngOnInit() {
    this.loadBarDataGlobal();
    this.loadBarDataCase1();
    this.loadBarDataCase2();
    this.loadBarDataCase3();
    this.loadBarDataCase4();
  }
  

  one(num:number){
    for(let i=0; i<5; i++){
      if(i===num){
        this.flags[i]=true;
      }else{
        this.flags[i]=false;
      }
      
    }
    return this.flags
  }
  
  loadBarDataGlobal(){
     this.barChartLabelsGlobal = ['2000','2001','2002','2003','2004','2005','2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014'];
     this.barChartDataGlobal = [
      {
        data: [0,	0,	0,	0,	1,	0,	1,	0,	3,	2,	3, 8, 4], 
        label: '5. Muy Alta'
      },
      {
        data: [31,	6,	5,	16,	42,	69,	35,	31,	53,	26,	45, 155, 107, 29, 57], 
        label: '4. Alta'
      },
      {
        data: [34,	5,	2,	17,	30, 45, 19, 35, 71, 39, 34, 157, 102, 51, 44], 
        label: '3. Media'
      },
      {
        data: [0,	0,	0,	0,	0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0], 
        label: '2. Baja'
      },
    ];

  }


  loadBarDataCase1(){
    this.barChartLabelsCase1 = ['2000','2001','2002','2003','2004','2005'];
    this.barChartDataCase1 = [
     {
       data: [0,	0,	0,	0,	1], 
       label: '5. Muy Alta'
     },
     {
       data: [31,	6,	5,	16,	42,	69], 
       label: '4. Alta'
     },
     {
       data: [34,	5,	2,	17,	30, 45], 
       label: '3. Media'
     },
     {
       data: [0,	0,	0,	0,	0, 0], 
       label: '2. Baja'
     },
   ];

   this.doughnutChartLabelsCase1 = ['5. Muy Alta', '4. Alta', '3. Media', '2. Baja'];
   this.doughnutChartDataCase1 = [1, 169, 133,0];
 }


 loadBarDataCase2(){
  this.barChartLabelsCase2 = ['2006', '2007', '2008', '2009', '2010'];
  this.barChartDataCase2 = [
   {
    data: [1,	0,	3,	2,	3], 
     label: '5. Muy Alta'
   },
   {
     data: [35,	31,	53,	26,	45], 
     label: '4. Alta'
   },
   {
     data: [19, 35, 71, 39, 34], 
     label: '3. Media'
   },
   {
     data: [0, 1, 2, 0, 0], 
     label: '2. Baja'
   },
   ];
   this.doughnutChartLabelsCase2 = ['5. Muy Alta', '4. Alta', '3. Media', '2. Baja'];
   this.doughnutChartDataCase2 = [1, 190, 198,3];

 }

 loadBarDataCase3(){
  this.barChartLabelsCase3 = ['2011', '2012'];
  this.barChartDataCase3 = [
   {
     data: [8,	4], 
     label: '5. Muy Alta'
   },
   {
     data: [155, 107], 
     label: '4. Alta'
   },
   {
     data: [157, 102], 
     label: '3. Media'
   },
   {
     data: [0, 0, 0, 0], 
     label: '2. Baja'
   },
 ];

 this.doughnutChartLabelsCase3 = ['5. Muy Alta', '4. Alta', '3. Media', '2. Baja'];
 this.doughnutChartDataCase3 = [12, 262, 259,0];
}

loadBarDataCase4(){
  this.barChartLabelsCase4 = [ '2013', '2014'];
  this.barChartDataCase4 = [
   {
     data: [0, 0], 
     label: '5. Muy Alta'
   },
   {
     data: [29, 57], 
     label: '4. Alta'
   },
   {
     data: [51, 44], 
     label: '3. Media'
   },
   {
     data: [0, 0], 
     label: '2. Baja'
   },
 ];

 this.doughnutChartLabelsCase4 = ['5. Muy Alta', '4. Alta', '3. Media','2. Baja'];
 this.doughnutChartDataCase4 = [0, 86, 95,0];

}
  
 
  




}
