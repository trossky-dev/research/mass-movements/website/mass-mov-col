import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-radar-graphic',
  templateUrl: './radar-graphic.component.html',
  styleUrls: ['./radar-graphic.component.css']
})
export class RadarGraphicComponent implements OnInit {

   // Radar
   public radarChartLabels:string[] = ['driving','Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];
   
    public radarChartData:any = [
      {data: [37,65, 59, 90, 81, 56, 55, 40], label: 'Luis'},
      {data: [70,28, 48, 40, 19, 96, 27, 100], label: 'Fernando'}
    ];
    public radarChartType:string = 'radar';


  @Input('case') caseNumber: number;
  constructor() { }

  ngOnInit() {
    console.log("Case "+this.caseNumber);
    
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }


}
