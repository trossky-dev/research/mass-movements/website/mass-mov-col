import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-pie-graphic',
  templateUrl: './pie-graphic.component.html',
  styleUrls: ['./pie-graphic.component.css']
})
export class PieGraphicComponent implements OnInit {

  @Input('case') caseNumber: number;
  constructor() { }

  ngOnInit() {
    console.log("Case "+this.caseNumber);
    
  }


}
