import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-doughnut-graphic',
  templateUrl: './doughnut-graphic.component.html',
  styleUrls: ['./doughnut-graphic.component.css']
})
export class DoughnutGraphicComponent implements OnInit {

  
  // Doughnut
  public doughnutChartLabels1:string[] = ['Luis', 'Fernando'];
  public doughnutChartData1:number[] = [50, 50];
  public doughnutChartType:string = 'doughnut';


  @Input('case') caseNumber: number;
  @Input() doughnutChartData:any[];
  @Input() doughnutChartLabels:any[];
  

  constructor() { }

  ngOnInit() {
    if(!this.doughnutChartData){
      this.doughnutChartData=this.doughnutChartData1

    }
    if(!this.doughnutChartLabels){
      this.doughnutChartLabels=this.doughnutChartLabels1
    }
  
    if(this.caseNumber){
      console.log("Case "+this.caseNumber);
    }
    
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
 

  numeros_random(){
   this.doughnutChartType = 'doughnut';
   this.doughnutChartData=[
    Math.round( Math.random()*100),
    Math.round( Math.random()*100),
    Math.round( Math.random()*100)
   ]

  }
}
