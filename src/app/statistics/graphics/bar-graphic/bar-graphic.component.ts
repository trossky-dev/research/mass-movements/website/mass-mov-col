import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bar-graphic',
  templateUrl: './bar-graphic.component.html',
  styleUrls: ['./bar-graphic.component.css']
})
export class BarGraphicComponent implements OnInit {
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartData1:any[]
  public barChartLabels1:string[]
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;

  @Input() barChartData:any[];
  @Input() barChartLabels:any[];

  @Input('case') caseNumber: number;

  constructor() { }

  ngOnInit() {
    if(!this.barChartData){
      this.barChartData=this.barChartData1

    }


    
    if(!this.barChartLabels){
      this.barChartLabels=this.barChartLabels1
    }
    if(this.caseNumber){
      console.log("Case "+this.caseNumber);
    }
   
  }




     // events
     public chartClicked(e:any):void {

      console.log(e);
    }
   
    public chartHovered(e:any):void {
      console.log(e);
    }
  

}
