import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-historic-graphic',
  templateUrl: './historic-graphic.component.html',
  styleUrls: ['./historic-graphic.component.css']
})
export class HistoricGraphicComponent implements OnInit {

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels1:string[] = ['2000','2001','2002','2003','2004','2005','2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;

  public barChartData1:any[] = [
    {
      data: [0,	0,	0,	0,	1,	0,	1,	0,	3,	2,	3, 8, 4], 
      label: '5. Muy Alta'
    },
    {
      data: [31,	6,	5,	16,	42,	69,	35,	31,	53,	26,	45, 155, 107, 29, 57], 
      label: '4. Alta'
    },
    {
      data: [34,	5,	2,	17,	30, 45, 19, 35, 71, 39, 34, 157, 102, 51, 44], 
      label: '3. Media'
    },
    {
      data: [0,	0,	0,	0,	0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0], 
      label: '2. Baja'
    },
  ];
 
    // Doughnut
     doughnutChartLabels1:string[] = ['5. Muy Alta', '4. Alta', '3. Media', '2. Baja'];
     doughnutChartData1:number[] = [22, 707, 686,3];
    

    @Input() doughnutChartLabels:string[];
    @Input() doughnutChartData:number[];
    @Input() barChartData:any[];
    @Input() barChartLabels:any[];
    @Input('case') caseNumber: number;


  constructor() { }
  
  ngOnInit() {

    if(!this.barChartData){
     
      this.barChartData=this.barChartData1

    }
    if(!this.barChartLabels){
      this.barChartLabels=this.barChartLabels1
    }

    if(!this.doughnutChartLabels){
      
      this.doughnutChartLabels=this.doughnutChartLabels1

    }
    if(!this.doughnutChartData){
      this.doughnutChartData=this.doughnutChartData1
    }


  }


    // events
    public chartClicked(e:any):void {
      console.log(e);
    }
   
    public chartHovered(e:any):void {
      console.log(e);
    }
  
    public randomize():void {
      // Only Change 3 values
      let data = [
        Math.round(Math.random() * 100),
        59,
        80,
        (Math.random() * 100),
        56,
        (Math.random() * 100),
        40];
      let clone = JSON.parse(JSON.stringify(this.barChartData));
      clone[0].data = data;
      this.barChartData = clone;
   
    }
}
