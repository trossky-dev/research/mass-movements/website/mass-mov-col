import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-line-graphic',
  templateUrl: './line-graphic.component.html',
  styleUrls: ['./line-graphic.component.css']
})
export class LineGraphicComponent implements OnInit {

  @Input('case') caseNumber: number;
  constructor() { }

  ngOnInit() {
    console.log("Case "+this.caseNumber);
    
  }

}
