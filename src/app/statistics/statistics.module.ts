import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NvD3Module } from 'ngx-nvd3';
import { ChartsModule } from 'ng2-charts';
import 'hammerjs';


/*
************************************************
*    Material modules for app
*************************************************
*/

import { MaterialModule } from '../app.material';
/*
************************************************
*     principal component
*************************************************
*/
import { GraphicsComponent } from './graphics/graphics.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
/*
************************************************
*     modules of  your app
*************************************************
*/


/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/
// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';
import { RadarGraphicComponent } from './graphics/radar-graphic/radar-graphic.component';
import { DoughnutGraphicComponent } from './graphics/doughnut-graphic/doughnut-graphic.component';
import { LineGraphicComponent } from './graphics/line-graphic/line-graphic.component';
import { BarGraphicComponent } from './graphics/bar-graphic/bar-graphic.component';
import { PieGraphicComponent } from './graphics/pie-graphic/pie-graphic.component';
import { HistoricGraphicComponent } from './graphics/historic-graphic/historic-graphic.component';

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule,
    NvD3Module,
    ChartsModule
    
  ],
  declarations: [GraphicsComponent, StatisticsComponent, ToolbarComponent, RadarGraphicComponent, DoughnutGraphicComponent, LineGraphicComponent, BarGraphicComponent, PieGraphicComponent, HistoricGraphicComponent]
})
export class StatisticsModule { }
