import { Routes } from '@angular/router';

import { StatisticsComponent }  from './statistics/statistics.component'
import { GraphicsComponent } from './graphics/graphics.component'




export const StatisticRouting: Routes= [

  { path:'statistics', component: StatisticsComponent, 
  children:[
    { path: '', redirectTo: 'graphics', pathMatch: 'full'},
    {
      path:'graphics', component: GraphicsComponent
    },
    {
      path:'tables', component: GraphicsComponent
    }
  ] },

]

